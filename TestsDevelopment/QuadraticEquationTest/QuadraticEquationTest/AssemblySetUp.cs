﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadraticEquationTest
{
    [SetUpFixture]
    public class AssemblySetUpClass
    {
        [OneTimeSetUp]
        public void AssemblySetUp()
        {
            Trace.WriteLine("Assembly setup...");
        }

        [OneTimeTearDown]
        public void AssemblyTeardown()
        {
            Trace.WriteLine("Assembly teardown...");
        }
    }
}
