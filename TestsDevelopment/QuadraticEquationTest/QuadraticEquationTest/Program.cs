﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadraticEquationTest
{
    class Program
    {

        //TestDriveDevelopment programming schema
        //1. Создается внешний вид готового кода
        //2. Негативный тест с заглушкой - показываем, как выглядит ошибка //a=4, b=4, c=2
        //3. Рефакторинг (меняем внешний вид кода - функции, классы, исключение дублирования...)

        //4. Позитивный тест с заглушкой - показывам, как выглядит успех
        //5. Рефакторинг  

        //6. Заглушка заменяется на реальный код (формула) для негативного теста
        //7. Заглушка заменяется на реальный код (формула) для позитивного теста
        //8. Рефакторинг тестов: эталонный лог тестирования

        static void Main(string[] args)
        {
            #region Simple Assert test
            /*double A = 3.0;
            try
            {
                Assert.AreEqual<double>(3.1, A, "A == 3.1");
            }
            catch (AssertFailedException ex)
            {
                Console.WriteLine(ex.Message);
            }*/
            #endregion




        }

        #region Реализация вручную
        /*
        static void Main(string[] args)
        {
            //TestDriveDevelopment programming schema
            //1. Создается внешний вид готового кода
            //2. Негативный тест с заглушкой - показываем, как выглядит ошибка //a=4, b=4, c=2
            //3. Рефакторинг (меняем внешний вид кода - функции, классы, исключение дублирования...)
            StringBuilder log = new StringBuilder();
            testIfDLessZero(log);
            //4. Позитивный тест с заглушкой - показывам, как выглядит успех
            //5. Рефакторинг  
            testIfDEqualZero(log);
            testIfDGreaterZero(log);
            //6. Заглушка заменяется на реальный код (формула) для негативного теста
            //7. Заглушка заменяется на реальный код (формула) для позитивного теста
            //8. Рефакторинг тестов: эталонный лог тестирования
            string etalonLog = "Корней нет. D<0. D==0. x1=-0.5, x2=-0.5 D>0. x1=3, x2=4 ";
            string msg = String.Empty;
            if (String.CompareOrdinal(etalonLog, log.ToString()) == 0)
            {
                msg = "Все тесты выполнены без ошибок";
            }


            Console.WriteLine(log.ToString());
        }

        private static void testIfDGreaterZero(StringBuilder log)
        {
            QuadraticEquation qa = new QuadraticEquation(1, -7, 12);
            double x1, x2;
            bool hasSolution = qa.calc(out x1, out x2);

            if (hasSolution == true)
            {
                log.Append("D>0. ");
                log.AppendFormat("x1={0}, x2={1} ", x1, x2);
            }
        }

        private static void testIfDEqualZero(StringBuilder log)
        {
            QuadraticEquation qa = new QuadraticEquation(4, 4, 2);
            double x1, x2;
            bool hasSolution = qa.calc(out x1, out x2);

            if (hasSolution == false)
            {
                log.Append("D==0. ");
                log.AppendFormat("x1={0}, x2={1} ", x1, x2);
            }
        }

        private static void testIfDLessZero(StringBuilder log)
        {
            QuadraticEquation qa = new QuadraticEquation(4, 4, 2);
            double x1, x2;
            bool hasSolution = qa.calc(out x1, out x2);
            
            if (hasSolution == false)
                log.Append("Корней нет. D<0. ");
        }
       
        */
        #endregion
    }
}
