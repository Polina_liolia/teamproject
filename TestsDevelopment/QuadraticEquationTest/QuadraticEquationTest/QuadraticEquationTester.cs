﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//NUnit tests
using NUnit.Framework;


namespace QuadraticEquationTest
{
    [TestFixture]
    public class QuadraticEquationTester
    {
        private QuadraticEquation qa;
        
        //before every test
        [SetUp]
        public void Setup ()
        {
            qa = new QuadraticEquation(4, 4, 2);
            bool flag = true;
            Assert.IsTrue(flag);
        }

        //after every test
        [TearDown]
        public void tearDown()
        {
            bool flag = true;
            Assert.IsTrue(flag);
        }

        #region Смысловые тесты
        [Test]
        [Order(0)]
        public void testIfDLessZero()
        {
            double x1, x2;
            bool hasSolution = qa.calc(out x1, out x2);
            Assert.IsTrue(hasSolution == false);
        }

        [Test]
        [Order(1)]
        public void testIfDGreaterZero()
        {
            double x1, x2;
            bool hasSolution = qa.calc(out x1, out x2);
            Assert.IsTrue(hasSolution == true);  
        }

        [Test]
        [Order(2)]
        public void testIfDEqualZero()
        {
            double x1, x2;
            bool hasSolution = qa.calc(out x1, out x2);
            Assert.IsTrue(hasSolution == false);
        }      
        #endregion
    }
}
