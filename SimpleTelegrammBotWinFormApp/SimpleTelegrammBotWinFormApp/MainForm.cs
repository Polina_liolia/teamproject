﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;

namespace SimpleTelegrammBotWinFormApp
{
    public partial class MainForm : Form
    {
        private static Telegram.Bot.TelegramBotClient BOT; //SimpleTellegramTest03_bot
        private string BotName;
        Telegram.Bot.Types.User me;
        private static string _token = "457256850:AAFlXJqWVebYQko5ZP2orbq_KwutPx6lEqA";

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            var text = txtKey.Text;
            if (text == null)
                text = _token;
            BOT = new TelegramBotClient(text);
            BOT.OnMessage += BotOnMessageReceived; //пришло сообщение от бота
            BOT.OnCallbackQuery += Bot_OnButton; //была нажата зарегистрированная кнопка
            BOT.SetWebhookAsync(); //запуск работы бота через обертку над WebAPI
            //работает кроссплатформенный механизм
            //метод StartMess... это работа напрямую к набору dll - использовать не рекомендуется

            me = BOT.GetMeAsync().Result; //принудительный первый опрос бота
            //знакомство - это необязательная часть
            BotName = me.Username;
            BOT.StartReceiving(); //запущен процесс постоянного асинхронного прослушивания
            btnRun.Enabled = false; //визуализация окончания процесса запуска бота через кнопку

        }

        private async void BotOnMessageReceived(object sender, MessageEventArgs e)
        {
            //сообщение от бота
            Telegram.Bot.Types.Message msg = e.Message;
            //реагирую только на тексты
            //не на видео, картинки, кнопку купить и т.д.
            if (msg == null || msg.Type != MessageType.TextMessage)
                return;
            StringBuilder sbAnswer = new StringBuilder();
            sbAnswer.AppendLine($"Здравствуй, я {BotName}!");
            switch (msg.Text)
            {
                case "/start":
                    {
                        string firstName = msg.From.FirstName;
                        string lastName = msg.From.LastName;
                        string userName = msg.From.Username;
                        sbAnswer.AppendLine($"{firstName} {lastName}, сыграем в игру 'Камень-ножницы-бумага'?");
                        sbAnswer.AppendLine($"/stone - твой камень");
                        sbAnswer.AppendLine($"/scissors - твои ножницы");
                        sbAnswer.AppendLine($"/paper - твоя бумага");
                      

                        var keyboard = new Telegram.Bot.Types.ReplyMarkups.InlineKeyboardMarkup(
                                       new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardButton[][]
                                       {
                                           // First row
                                           new [] {
                                               // First column
                                               new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardCallbackButton("Да?","callback1"),

                                               // Second column
                                               new Telegram.Bot.Types.InlineKeyboardButtons.InlineKeyboardCallbackButton("Нет?","callback2"),
                                           },
                                       });
                        await BOT.SendTextMessageAsync(msg.Chat.Id, sbAnswer.ToString(), replyMarkup: keyboard);
                        return;
                    }
                case "/scissors":
                    sbAnswer.AppendLine("А у меня камень - ты проиграл!");
                    break;
                case "/stone":
                    sbAnswer.AppendLine("А у меня бумага - ты проиграл!");
                    break;
                case "/paper":
                    sbAnswer.AppendLine("А у меня ножницы - ты проиграл!");
                    break;
                default: break;
            }
            await BOT.SendTextMessageAsync(msg.Chat.Id, sbAnswer.ToString());
        }

        private async void Bot_OnButton(object sender, CallbackQueryEventArgs e)
        {
            var message = e.CallbackQuery.Message;
            if (e.CallbackQuery.Data == "callback1")
            {
                await BOT.AnswerCallbackQueryAsync(e.CallbackQuery.Id, @"/stone - твой камень\r\n/scissors - твои ножницы\r\n/paper - твоя бумага\r\n" + e.CallbackQuery.Data, true);
            }
            else
                if (e.CallbackQuery.Data == "callback2")
            {
                await BOT.SendTextMessageAsync(message.Chat.Id, "тест", replyToMessageId: message.MessageId);
                await BOT.AnswerCallbackQueryAsync(e.CallbackQuery.Id); // отсылаем пустое, чтобы убрать "частики" на кнопке
            }
        }


    }
}
